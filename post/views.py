from django.shortcuts import render

from .models import Post


def get_posts(request):
    posts = Post.objects.all()
    return render(
        request=request,
        template_name='post/posts.html',
        context={'posts': posts}
    )
