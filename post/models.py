from django.db import models


class Post(models.Model):
    AUTHOR_MAX_LENGTH = 255
    author = models.CharField(
        max_length=AUTHOR_MAX_LENGTH,
        null=False
    )

    TITLE_MAX_LENGTH = 255
    title = models.CharField(
        max_length=TITLE_MAX_LENGTH,
        null=False
    )

    content = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta(object):
        unique_together = ('author', 'title')

    def __str__(self):
        return self.author + ":" + self.title
